import asyncio
import logging
import time

from itertools import cycle
from unittest import TestCase
from uuid import uuid4

from piper.comm import Channel, Subscriber
from piper.comm.close_condition import CloseCondition
from piper.exceptions import YieldClosedException
from piper.utils.test_helpers import async_close_channels_callback


class TestChannel(TestCase):
    def setUp(self):
        self._loop = asyncio.new_event_loop()
        self._loop.set_debug(True)

        self.sub_in = list()
        self.sub_out = list()

        self.channel = Channel(["data"])

    def tearDown(self):
        time.sleep(2)
        self._loop.stop()
        self._loop.close()

    def test_has_inputs(self):
        assert not self.channel.has_inputs()
        self._setup_subscribers(1, 2)
        assert self.channel.has_inputs()

    def test_one_to_one_flow(self):
        id_tag, data = uuid4(), self._basic_data_package()

        self._setup_subscribers(1, 1)
        results = self._run_test([(id_tag, data)])

        self._assert_data_points(results, {id_tag: data}, 1)

    def test_one_to_many_flow(self):
        id_tag1, data1 = uuid4(), self._basic_data_package("data2")
        id_tag2, data2 = uuid4(), self._basic_data_package("data1")

        self._one_to_many(((id_tag1, data1), (id_tag2, data2)), 2)

    def test_one_to_many_flow_bcast(self):
        self.channel = Channel(["data"], True)

        id_tag, data = uuid4(), self._basic_data_package()

        self._one_to_many(((id_tag, data),), 2)

    def test_many_to_one_flow(self):
        id_tag1, data1 = uuid4(), self._basic_data_package("data2")
        id_tag2, data2 = uuid4(), self._basic_data_package("data1")

        self._many_to_one(((id_tag1, data1), (id_tag2, data2)), n=2)

    def test_many_to_one_flow_partial(self):
        self.channel = Channel(["data1", "data2"])

        id_tag = uuid4()
        data1, data2 = {"data1": "data1"}, {"data2": "data2"}

        self._many_to_one(
            ((id_tag, data1), (id_tag, data2)),
            ((id_tag, {**data1, **data2}),),
            1
        )

    def test_many_to_many_flow(self):
        logging.basicConfig(level="DEBUG")
        id_tag1, data1 = uuid4(), self._basic_data_package("data1")
        id_tag2, data2 = uuid4(), self._basic_data_package("data2")

        self._many_to_many(((id_tag1, data1), (id_tag2, data2)), n=2)

    def test_many_to_many_flow_bcast(self):
        self.channel = Channel(["data"], True)
        id_tag1, data1 = uuid4(), self._basic_data_package("data2")
        id_tag2, data2 = uuid4(), self._basic_data_package("data1")

        self._many_to_many(
            ((id_tag1, data1), (id_tag2, data2)), n=4
        )

    def test_many_to_many_flow_partial(self):
        self.channel = Channel(["data1", "data2"])

        id_tag1, id_tag2 = uuid4(), uuid4()
        data11, data12 = {"data1": "data11"}, {"data2": "data12"}
        data21, data22 = {"data1": "data21"}, {"data2": "data22"}

        self._many_to_many(
            (
                (id_tag1, data11), (id_tag1, data12),
                (id_tag2, data21), (id_tag2, data22),
            ),
            (
                (id_tag1, {**data11, **data12}),
                (id_tag2, {**data21, **data22})
            ),
            2
        )

    def test_many_to_many_flow_partial_bcast(self):
        self.channel = Channel(["data1", "data2"], True)

        id_tag1, id_tag2 = uuid4(), uuid4()
        data11, data12 = {"data1": "data11"}, {"data2": "data12"}
        data21, data22 = {"data1": "data21"}, {"data2": "data22"}

        self._many_to_many(
            (
                (id_tag1, data11), (id_tag1, data12),
                (id_tag2, data21), (id_tag2, data22),
            ),
            (
                (id_tag1, {**data11, **data12}),
                (id_tag2, {**data21, **data22})
            ),
            4
        )

    def _setup_subscribers(self, n_in, n_out):
        self.sub_in = list(Subscriber(
            name="Sub_in{}".format(i)
        ) for i in range(n_in))

        self.sub_out = list(Subscriber(
            name="Sub_out{}".format(i)
        ) for i in range(n_out))

        for sub in self.sub_in:
            self.channel.add_subscriber(sub, Channel.Sub.IN)

        for sub in self.sub_out:
            self.channel.add_subscriber(sub, Channel.Sub.OUT)

    def _run_test(self, data_points):
        end_cnd = CloseCondition()

        self.channel.start(lambda: end_cnd)

        trans = self._loop.create_task(
            self._transmit_data(data_points)
        )
        trans.add_done_callback(
            async_close_channels_callback(
                self._close_connections,
                self._loop,
                end_cnd
            )
        )

        results = self._loop.create_task(
            self._get_results()
        )
        self._loop.run_until_complete(results)

        return results.result()

    def _one_to_many(self, data_points, n):
        self._setup_subscribers(1, 2)
        results = self._run_test(data_points)
        self._assert_data_points(results, dict(data_points), n)

    def _many_to_one(self, data_points, ret_data=None, n=2):
        self._setup_subscribers(2, 1)
        results = self._run_test(data_points)
        self._assert_data_points(
            results, dict(ret_data if ret_data else data_points), n
        )

    def _many_to_many(self, data_points, ret_data=None, n=0):
        self._setup_subscribers(2, 2)
        results = self._run_test(data_points)
        self._assert_data_points(
            results, dict(ret_data if ret_data else data_points), n
        )

    async def _transmit_data(self, data_points):
        for trans in asyncio.as_completed([
            sub.transmit(*data)
            for sub, data in zip(cycle(self.sub_in), data_points)
        ]):
            await trans

    async def _close_connections(self):
        for fut in asyncio.as_completed([
            s.shutdown() for s in self.sub_in
        ]):
            await fut

    async def _get_results(self):
        results = []

        while any(list(s.promise_data() for s in self.sub_out)):
            for fut in asyncio.as_completed([
                o.yield_data() for o in filter(
                    lambda s: s.promise_data(), self.sub_out
                )
            ]):
                try:
                    results.append(await fut)
                except asyncio.CancelledError:
                    pass
                except YieldClosedException:
                    pass
                except Exception as e:
                    raise e

        return results

    def _basic_data_package(self, value="data"):
        return {"data": value}

    def _assert_data_points(self, data, awaited_data, awaited_n):
        i = 0
        for id_tag, dt in data:
            assert id_tag in awaited_data.keys()
            assert awaited_data[id_tag] == dt
            i += 1

        assert i == awaited_n, "{} data points received versus {}".format(
            i, awaited_n
        )
