import asyncio
import logging
import time

from unittest import TestCase
from uuid import uuid4

from piper.comm import Subscriber, Channel


class TestCommIntensive(TestCase):
    def _giganto_diamond(self):
        # TODO : Fix shape of diamond

        n_levels = 5
        sub_in = Subscriber("sub_in")
        sub_out = Subscriber("sub_out")
        channels_w = [Channel(["item"], True, "chan_11")]
        channels_w[-1].add_subscriber(sub_in)

        channels_s = [Channel(["item"], name="chan_{}1".format(n_levels))]
        channels_s[-1].add_subscriber(sub_out, Channel.Sub.OUT)

        for i in range(2, int(n_levels / 2) + 1):
            # Create widening part of diamond
            layer_w_input_subs = []
            layer_s_output_subs = []
            chan_rng = sum(range(i - 1))

            # Bind last layer output while creating subscribers for next one
            for j in range(1, i):
                # Widening
                print(i, j)
                layer_w_input_subs.append(
                    Subscriber("sub_{}{}_to_{}{}".format(i - 1, j, i, j))
                )
                layer_w_input_subs.append(
                    Subscriber("sub_{}{}_to_{}{}".format(i - 1, j, i, j + 1))
                )
                channels_w[chan_rng + j - 1].add_subscriber(
                    layer_w_input_subs[-1], Channel.Sub.OUT
                )
                channels_w[chan_rng + j - 1].add_subscriber(
                    layer_w_input_subs[-2], Channel.Sub.OUT
                )

                # Gathering
                layer_s_output_subs.append(
                    Subscriber("sub_{}{}_to_{}{}".format(
                        n_levels - i - 1, j, n_levels - i, j)
                    )
                )
                layer_s_output_subs.append(
                    Subscriber("sub_{}{}_to_{}{}".format(
                        n_levels - i - 1, j, n_levels - i, j + 1)
                    )
                )
                channels_s[chan_rng + j - 1].add_subscriber(
                    layer_s_output_subs[-1]
                )
                channels_s[chan_rng + j - 1].add_subscriber(
                    layer_s_output_subs[-2]
                )

            layer_w_input_subs = iter(layer_w_input_subs)
            layer_s_output_subs = iter(layer_s_output_subs)
            # Bind subscriber to new layer while building it
            channels_w.append(Channel(["item"], True, "chan_{}1".format(i)))
            channels_w[-1].add_subscriber(next(layer_w_input_subs))
            channels_s.append(
                Channel(["item"], True, "chan_{}1".format(n_levels - i))
            )
            channels_s[-1].add_subscriber(
                next(layer_s_output_subs), Channel.Sub.OUT
            )

            for j in range(2, i):
                channels_w.append(
                    Channel(["item", True, "chan_{}{}".format(i, j)])
                )
                channels_w[-1].add_subscriber(next(layer_w_input_subs))
                channels_w[-1].add_subscriber(next(layer_w_input_subs))

                channels_s.append(Channel(
                    ["item"], True, "chan_{}{}".format(n_levels - i, j)
                ))
                channels_s[-1].add_subscriber(
                    next(layer_s_output_subs), Channel.Sub.OUT
                )
                channels_s[-1].add_subscriber(
                    next(layer_s_output_subs), Channel.Sub.OUT
                )

            channels_w.append(
                Channel(["item", True, "chan_{}{}".format(i, i)])
            )
            channels_w[-1].add_subscriber(next(layer_w_input_subs))

            channels_s.append(
                Channel(["item", True, "chan_{}{}".format(n_levels - i, i)])
            )
            channels_s[-1].add_subscriber(next(layer_s_output_subs))

        # Create middle layer and connect to both parts (this is done to ensure
        # no duplicate of the middle part, as well as a proper diamond if an
        # even number of layers is supplied
        i = int(n_levels / 2) + 1
        start_channels = sum(range(i - 1)) - 1
        mid_channels = [Channel(["item"], True, "chan_{}1".format(i))]
        sub = Subscriber("sub_{}{}_to_{}{}".format(i - 1, 1, i, 1))
        mid_channels[-1].add_subscriber(sub)
        channels_w[start_channels].add_subscriber(sub, Channel.Sub.OUT)
        sub = Subscriber("sub_{}{}_to_{}{}".format(i, 1, i + 1, 1))
        channels_s[start_channels].add_subscriber(sub)
        mid_channels[-1].add_subscriber(sub, Channel.Sub.OUT)

        for j in range(2, i):
            mid_channels.append(
                Channel(["item"], True, "chan_{}{}".format(i, j))
            )
            sub = Subscriber("sub_{}{}_to_{}{}".format(i - 1, j - 1, i, j))
            mid_channels[-1].add_subscriber(sub)
            channels_w[start_channels + j - 2].add_subscriber(
                sub, Channel.Sub.OUT
            )
            sub = Subscriber("sub_{}{}_to_{}{}".format(i - 1, j, i, j))
            mid_channels[-1].add_subscriber(sub)
            channels_w[start_channels + j - 1].add_subscriber(
                sub, Channel.Sub.OUT
            )

            sub = Subscriber("sub_{}{}_to_{}{}".format(i, j - 1, i + 1, j))
            mid_channels[-1].add_subscriber(sub, Channel.Sub.OUT)
            channels_s[start_channels + j - 2].add_subscriber(sub)
            sub = Subscriber("sub_{}{}_to_{}{}".format(i, j, i + 1, j))
            mid_channels[-1].add_subscriber(sub, Channel.Sub.OUT)
            channels_s[start_channels + j - 1].add_subscriber(sub)

        mid_channels.append(Channel(["item"], True, "chan_{}{}".format(i, i)))
        sub = Subscriber("sub_{}{}_to_{}{}".format(i - 1, i, i, i))
        mid_channels[-1].add_subscriber(sub)
        channels_w[-1].add_subscriber(sub, Channel.Sub.OUT)
        sub = Subscriber("sub_{}{}_to_{}{}".format(i, i, i + 1, i))
        channels_s[-1].add_subscriber(sub)
        mid_channels[-1].add_subscriber(sub, Channel.Sub.OUT)

        self._run_test(channels_w + mid_channels + channels_s, sub_in, sub_out)

    def test_1000_channel_chain_fn(self):
        sub_in = Subscriber("sub_in")
        sub_out = Subscriber("sub_out")

        channels = [Channel(["item"], name="chan_1")]
        channels[0].add_subscriber(sub_in)

        for i in range(2, 1001):
            between_sub = Subscriber("sub_{}_to_{}".format(i - 1, i))
            channels[-1].add_subscriber(between_sub, Channel.Sub.OUT)
            channels.append(Channel(["item"], name="chan_{}".format(i)))
            channels[-1].add_subscriber(between_sub)

        channels[-1].add_subscriber(sub_out, Channel.Sub.OUT)

        self._run_test(channels, sub_in, sub_out)

    def _run_test(self, channels, sub_in, sub_out):
        loop = asyncio.new_event_loop()
        start = time.time()

        for channel in channels:
            channel.start()

        loop.create_task(
            sub_in.transmit(uuid4(), {"item": 0})
        ).add_done_callback(
            lambda *args: loop.create_task(sub_in.shutdown())
        )

        result_task = loop.create_task(self._dequeue(sub_out))
        loop.run_until_complete(result_task)
        logger.info("Obtained {} results in {} seconds".format(
            len(result_task.result()), time.time() - start)
        )

    async def _dequeue(self, sub):
        results = []
        while sub.promise_data():
            try:
                results.append(await sub.yield_data())
            except asyncio.CancelledError:
                pass

        return results


logger = logging.getLogger(TestCommIntensive.__name__)
