import importlib
import json
from os import getcwd, remove
from os.path import join
from tempfile import NamedTemporaryFile

from unittest import TestCase


class TestTraitletsConfig(TestCase):
    def setUp(self):
        self.config_module = importlib.import_module('.config', 'piper.utils')
        self.config_manager = getattr(self.config_module, 'config_manager')
        self.piper_config = getattr(self.config_module, 'piper_config')
        self._garbage_files_collect = []

    def test_config_inits_well(self):
        assert self.piper_config is not None
        assert isinstance(
            self.piper_config, getattr(self.config_module, 'PiperConfig')
        )
        assert isinstance(
            self.config_manager,
            getattr(self.config_module, 'PiperConfigApplication')
        )

    def test_swap_config(self):
        config = {
            "PiperConfig": {
                "neo4j_db_connection": {
                    "username": "dummy_user",
                    "password": "dummy_password",
                    "address": ("123.456.78.90", 1234)
                },
                "singularity": {
                    "image": join(getcwd(), "dummy.simg")
                }
            }
        }
        with open(join(getcwd(), "dummy.simg"), 'a') as simg_file:
            self._garbage_files_collect.append(simg_file.name)
            with NamedTemporaryFile(
                mode="w", dir=getcwd(), suffix=".json", delete=False
            ) as config_file:
                config_file.write(json.dumps(config))
                conf_name = config_file.name
                self._garbage_files_collect.append(conf_name)

            conf_man_klass = getattr(
                self.config_module, 'PiperConfigApplication'
            )
            conf_man_klass.load_config_dynamically(conf_name)
            new_config = self.piper_config.to_json_configuration()
            assert 'PiperConfig' in new_config
            self._assert_dict_config(
                config['PiperConfig'], new_config['PiperConfig']
            )

    def _assert_dict_config(self, config, cmp_config):
        for k, v in config.items():
            assert k in cmp_config
            if isinstance(v, dict):
                assert isinstance(cmp_config[k], dict)
                self._assert_dict_config(v, cmp_config[k])
            else:
                assert cmp_config[k] == v, "{} : {} not equal to {}".format(
                    k, cmp_config[k], v
                )

    def tearDown(self):
        self.piper_config = None
        self.config_manager = None
        del self.config_module
        for item in self._garbage_files_collect:
            remove(item)
