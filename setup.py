from setuptools import setup, find_packages


_CLASSIFIERS = [
    "Development Status :: 3 - Alpha",
    "Environment :: Console",
    "Framework :: AsyncIO",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: GNU General Public License (GPL)",
    "Natural Language :: English",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3.7",
    "Topic :: Scientific/Engineering",
    "Topic :: Software Development",
    "Topic :: Software Development :: Libraries :: Application Frameworks",
    "Topic :: Software Development :: Libraries :: Python Modules",
    "Topic :: System :: Distributed Computing",
    "Topic :: Utilities"
]


with open("README.md", encoding="utf-8") as ldf:
    long_description = ldf.read()

setup(
    name='piper',
    packages=find_packages(exclude=('test', 'test.*', '*.test.*')),
    author='Alex Valcourt Caron',
    author_email='alex.valcourt.caron@usherbrooke.ca',
    classifiers=_CLASSIFIERS,
    description='Pure python pipelining system built on asyncio',
    license='GNU-GPL',
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires="==3.7.*",
    setup_requires=['setuptools_scm'],
    install_requires=[
        'importlib-metadata ~= 1.0 ; python_version < "3.8"',
        'neomodel ~= 3.3.2',
        'setuptools ~= 49.2.0',
        'traitlets ~= 4.3.3',
        'psutil ~= 5.7.2'
    ],
    use_scm_version=True
)
