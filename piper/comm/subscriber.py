import asyncio
import logging
from asyncio import Queue
from functools import partial

from piper.drivers.asyncio import AsyncLoopManager
from piper.exceptions import TransmitClosedException, \
                             YieldClosedException, \
                             AlreadyShutdownException


class Subscriber(AsyncLoopManager):
    def __init__(self, name="sub", includes=None, excludes=None):
        super().__init__(name=name)

        assert includes is None or excludes is None, \
            "Only supply one of both includes or excludes"

        if includes is not None:
            self._filter_data = partial(
                self._filter_includes, includes=includes
            )

        if excludes is not None:
            self._filter_data = partial(
                self._filter_excludes, excludes=excludes
            )

        self._timestamp = None
        self._queue = {}
        self._alive = True
        self._depth = 0
        ready_evt = self.start()
        ready_evt.wait()
        self._id_tags = Queue(loop=self._task_manager.loop)
        self._transmit_tasks = []
        self._yield_tasks = []

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, val):
        self._depth = val

    def promise_data(self):
        return (self.is_alive() or self.data_ready()) and not self._closing

    def is_alive(self):
        return self._alive

    def data_ready(self):
        return not self._id_tags.empty()

    def timestamp(self, timestamp):
        if self._timestamp == timestamp:
            return False

        self._timestamp = timestamp
        return True

    async def transmit(self, id_tag, package):
        logger.debug("{} transmitting".format(self._name))

        if id_tag not in self._queue:
            self._queue[id_tag] = self._filter_data(package)

            task = self._task_manager.create_task(self._id_tags.put(id_tag))

            self._transmit_tasks.append(task)
            await task
            self._transmit_tasks.remove(task)

        else:
            self._queue[id_tag].update(self._filter_data(package))

        logger.debug("{} has finished transmitting".format(self._name))

    async def yield_data(self):
        task = None
        try:
            logger.debug("{} fetching data".format(self._name))
            task = self._task_manager.create_task(self._inner_yield())
            result = await task

            logger.debug("{} received data".format(self._name))

            return result
        except asyncio.CancelledError:
            if task:
                self._task_manager.protect_current_task()
                task.cancel()
                await task
        except RuntimeError as e:
            if self._task_manager.is_closed():
                logger.debug("{} won't receive data".format(self._name))
                raise asyncio.CancelledError(self)
            else:
                raise e

    async def shutdown(self, force=False):
        try:
            self._alive = False
            self._task_manager.protect_current_task()
            shutdown_fn = self._inner_shutdown
            self._inner_shutdown = self._already_shut_bypass
            await self._task_manager.create_task(shutdown_fn(force))
        except AlreadyShutdownException:
            pass

    async def wait_for_dequeuing(self):
        logger.debug("{} waiting for dequeue : {} ids in queue".format(
            self._name, self._id_tags.qsize()
        ))

        await self._task_manager.create_protected_task(self._id_tags.join())
        self.yield_data = self._shutdown_yield_data

        logger.debug("{} dequeued".format(self._name))

    def _filter_data(self, data):
        return data

    def _filter_includes(self, data, includes):
        return dict(filter(lambda d: d[0] in includes, data.items()))

    def _filter_excludes(self, data, excludes):
        return dict(filter(lambda d: d[0] not in excludes, data.items()))

    async def _inner_yield(self):
        task = asyncio.current_task(self._task_manager.loop)
        self._yield_tasks.append(task)
        id_tag = await self._id_tags.get()
        self._id_tags.task_done()
        self._yield_tasks.remove(task)
        return id_tag, self._queue.pop(id_tag)

    async def _inner_shutdown(self, force):
        try:
            if not self._closing:
                self._closing = force
                logger.debug("{} shutdown initiated : forced={}".format(
                    self._name, force)
                )
                self.transmit = self._shutdown_transmit_data

                if force:
                    self.yield_data = self._shutdown_yield_data
                else:
                    for fut in asyncio.as_completed(self._transmit_tasks):
                        await fut

                    await self._task_manager.create_task(
                        self.wait_for_dequeuing()
                    )

                    self.yield_data = self._shutdown_yield_data

                self._closing = True

                logger.debug("{} cancelling tasks".format(self._name))
                self._task_manager.protect_current_task()
                await self._task_manager.create_protected_task(
                    self._cancel_running_tasks()
                )

                logger.debug("{} closing loop".format(self._name))
                self.close_loop()
        except RuntimeError:
            pass

    async def _already_shut_bypass(self, *args, **kwargs):
        raise AlreadyShutdownException(self)

    async def _shutdown_yield_data(self):
        logger.debug("{} yield has been shutdown".format(self._name))
        raise YieldClosedException(self)

    async def _shutdown_transmit_data(self, *args, **kwargs):
        logger.debug(
            "{} was asked to transmit but has shutdown".format(self._name)
        )
        raise TransmitClosedException(self)

    # No need of execute async for the subscriber class, it is not looping,
    # only relaying information from one loop to another. Could be interesting
    # though if some logic on the subs ever need to be auto-managed
    async def _execute_async(self, *args, **kwargs):
        pass


logger = logging.getLogger(Subscriber.__name__)
