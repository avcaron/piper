from os import getcwd

from traitlets import Unicode, List, default, Bool, Integer, observe
from traitlets.config import SingletonConfigurable, Application

from piper import __version__

from piper.drivers.traitlets import BoltConnection, \
                                    ExistingFile, \
                                    SelfInstantiatingInstance, \
                                    ExistingDirectory, \
                                    SSHConnection, \
                                    PiperConfigurable


class ShellLoggingConfiguration(PiperConfigurable):
    overwrite = Bool(default_value=True).tag(config=True)
    sleep = Integer(default_value=4).tag(config=True)

    def shell_dict(self):
        return {
            "overwrite": self.overwrite,
            "sleep": self.sleep
        }

    def to_json_configuration(self):
        return self.shell_dict()


class SingularityConfiguration(PiperConfigurable):
    image = ExistingFile(
        default_value=None, allow_none=True
    ).tag(config=True)
    bind_paths = List(
        trait=ExistingDirectory(), default_value=[]
    ).tag(config=True)
    enabled = Bool(default_value=False)

    @observe('image')
    def enabled_if_not_none(self, change):
        self.enabled = change['new'] is not None

    def __str__(self):
        return "singularity exec -B {} {}".format(
            ",".join(self.bind_paths), self.image
        )

    def to_json_configuration(self):
        return {
            'image': str(self.image),
            'bind_paths': str(self.bind_paths)
        }


class ShellCommandFormatter(PiperConfigurable):
    def format_shell(self, shell_exec_string):
        return shell_exec_string

    def to_json_configuration(self):
        return {}


class SSHShellCommandFormatter(ShellCommandFormatter):
    connection = SelfInstantiatingInstance(SSHConnection).tag(config=True)

    def format_shell(self, shell_exec_string):
        return "{} {}".format(self.connection, shell_exec_string)

    def to_json_configuration(self):
        return {
            'connection': self.connection.to_json_configuration()
        }


class PiperConfig(SingletonConfigurable):
    neo4j_db_connection = SelfInstantiatingInstance(
        BoltConnection, kw={
            'username': 'neo4j',
            'password': 'neo4j',
            'address': ('localhost', 7687),
            'encrypted': False
        }
    ).tag(config=True)

    shell_logging = SelfInstantiatingInstance(
        ShellLoggingConfiguration
    ).tag(config=True)

    singularity = SelfInstantiatingInstance(
        SingularityConfiguration
    ).tag(config=True)

    shell_formatter = SelfInstantiatingInstance(
        ShellCommandFormatter
    ).tag(config=True)

    debug = Bool(default_value=False).tag(config=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.neo4j_db_connection.parent = self
        self.shell_logging.parent = self
        self.singularity.parent = self
        self.shell_formatter.parent = self

    def to_json_configuration(self):
        return {
            'PiperConfig': {
                'neo4j_db_connection':
                    self.neo4j_db_connection.to_json_configuration(),
                'shell_logging': self.shell_logging.to_json_configuration(),
                'singularity': self.singularity.to_json_configuration(),
                'shell_formatter': self.shell_formatter.to_json_configuration()
            }
        }

    def generate_shell_log_config(self):
        return self.shell_logging.shell_dict()

    def format_shell_executable(self, shell_exec_string):
        if self.singularity.enabled:
            return "{} {}".format(self.singularity, shell_exec_string)

        return shell_exec_string


class PiperConfigApplication(Application):
    name = Unicode(u'piper configuration service')
    description = Unicode(u'Small underlying service to piper pipelining '
                          u'utility, propagating global configuration '
                          u'options to sub-packages')
    # TODO : change this version to follow setup.py and global version of
    #        package when it will be exported to its own project
    version = Unicode(u'{}'.format(__version__))
    classes = List()

    @default('classes')
    def _classes_default(self):
        return [
            self.__class__, PiperConfig
        ]

    def initialize(self, argv=None):
        global piper_config
        super().initialize(argv)
        piper_config = PiperConfig.instance(parent=self)

    def start(self):
        self.load_config_file("piper_config", getcwd())
        piper_config.update_config(self.config)
        super().start()

    @classmethod
    def load_config_dynamically(cls, config_file):
        config_manager.load_config_file(config_file)
        piper_config.update_config(config_manager.config)


piper_config = None
PiperConfigApplication.launch_instance()
config_manager = PiperConfigApplication.instance()
