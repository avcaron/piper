

class MetadataManager:
    def __init__(self, default_value=None):
        self._library = {}
        self._default = default_value

    def register_category(
        self, holder_key, metadata, constructor=lambda meta, manager: meta
    ):
        assert holder_key not in self._library
        self._library[holder_key] = (metadata, constructor)

    def get_category(self, holder_key):
        try:
            metadata, constructor = self._library[holder_key]
            return constructor(metadata, self)
        except KeyError:
            return self._default
