import asyncio
import logging
from abc import ABCMeta
from functools import partial

from piper.comm import Channel, CloseCondition, Subscriber, ChannelFilter
from piper.drivers.asyncio import AsyncLoopManager
from piper.exceptions import RecoverableException, UnrecoverableException

from .pipeline_item import PipelineItem, connect_pipeline_items


class Layer(PipelineItem, AsyncLoopManager, metaclass=ABCMeta):
    async def _execute_async(self, *args, **kwargs):
        pass

    def __init__(
        self, input_channel, output_channel, name="layer"
    ):
        AsyncLoopManager.__init__(self, name=name)
        PipelineItem.__init__(self, input_channel, output_channel, name)
        self._layer = []
        self._hidden_connections = []
        self._end_cnd = CloseCondition()

    @property
    def serialize(self):
        return {
            **AsyncLoopManager.serialize.fget(self),
            **PipelineItem.serialize.fget(self),
            **{
                'inter_item_channels': [
                    c.serialize for c in self._connections[1:-1]
                ],
                'hidden_channels': [
                    c.serialize for c in self._hidden_connections
                ],
                'items': [i.serialize for i in self._layer]
            }
        }

    @property
    def package_keys(self):
        return self._connections[-1].package_keys

    def initialize(self, exception_handler=None, depth=0, **kwargs):
        ex_handler = partial(
            self._async_exception_handler, basic_handler=exception_handler
        )
        ready_evt = AsyncLoopManager.start(
            self, exception_handler=ex_handler
        )
        ready_evt.wait()

        self._start_channels(ex_handler, depth)

        for item in self._layer:
            item.initialize(ex_handler, depth)

        self._initialized = True
        PipelineItem.initialize(self, depth=depth)

    def add_unit(
        self, unit, additional_inputs=(), additional_outputs=(), bcast=False
    ):
        self._layer.append(unit)

        if len(additional_inputs) > 0:
            in_channel = Channel(
                unit.package_keys, name="{}_input".format(unit.name)
            )
            unit.connect_input(in_channel)
            for parent in additional_inputs:
                includes, excludes = None, None
                if isinstance(parent, ChannelFilter):
                    parent, includes, excludes = parent.get_filter_on_item()

                if isinstance(parent, PipelineItem):
                    parent.connect_output(in_channel, includes, excludes)
                elif isinstance(parent, Channel):
                    sub = Subscriber(
                        "{}_to_{}".format(parent.name, unit.name),
                        includes, excludes
                    )
                    parent.add_subscriber(sub, Channel.Sub.OUT)
                    in_channel.add_subscriber(sub)

            self._hidden_connections.append(in_channel)

        if len(additional_outputs) > 0:
            out_channel = Channel(
                unit.package_keys, name="{}_input".format(unit.name),
                broadcast_out=bcast
            )
            unit.connect_output(out_channel)
            for child in additional_outputs:
                includes, excludes = None, None
                if isinstance(child, ChannelFilter):
                    child, includes, excludes = child.get_filter_on_item()

                if isinstance(child, PipelineItem):
                    child.connect_input(out_channel, includes, excludes)
                else:
                    sub = Subscriber(
                        "{}_to_{}".format(unit.name, child.name),
                        includes, excludes
                    )
                    out_channel.add_subscriber(sub, Channel.Sub.OUT)
                    child.add_subscriber(sub)

            self._hidden_connections.append(out_channel)

    def connect_input(self, channel, includes=None, excludes=None):
        subscriber = Subscriber(
            name="sub_{}_to_{}".format(channel.name, self.input.name),
            includes=includes, excludes=excludes
        )
        channel.add_subscriber(subscriber, Channel.Sub.OUT)
        self.input.add_subscriber(subscriber, Channel.Sub.IN)
        return PipelineItem.connect_input(self, channel)

    def connect_output(self, channel, includes=None, excludes=None):
        subscriber = Subscriber(
            name="sub_{}_to_{}".format(self.output.name, channel.name),
            includes=includes, excludes=excludes
        )
        channel.add_subscriber(subscriber, Channel.Sub.IN)
        self.output.add_subscriber(subscriber, Channel.Sub.OUT)
        return PipelineItem.connect_output(self, channel)

    def set_test(self, on=True):
        PipelineItem.set_test(self, on)
        for item in self._layer:
            item.set_test(on)

    async def process(self):
        assert self._initialized, "{} uninitialized".format(self._name)

        await self._task_manager.create_task(self._process())

        self._end_cnd.set()
        AsyncLoopManager.close_loop(self)

    def _start_channels(self, exception_handler, depth):
        for channel in self._connections + self._hidden_connections:
            channel.start(
                lambda: self._end_cnd,
                exception_handler=exception_handler, depth=depth
            )

    def _async_exception_handler(self, loop, context, basic_handler=None):
        if not self._closing:
            self._closing = True
            ex = context['exception'] if 'exception' in context else None

            if isinstance(ex, RecoverableException):
                logger.exception("{} got a recoverable exception, {}".format(
                    self.name,
                    "processing will continue"
                ))
                return
            elif isinstance(ex, UnrecoverableException):
                logger.exception("{} got an unrecoverable exception".format(
                    self.name
                ))

                logger.warning(
                    "{} will attempt graceful shutdown".format(self.name)
                )

                task = self._task_manager.create_protected_task(
                    self._shut_channels()
                )
                task.add_done_callback(
                    lambda *args: AsyncLoopManager._async_exception_handler(
                        self, loop, context, basic_handler
                    )
                )

    async def _process(self):
        for t in asyncio.as_completed(
            [unit.process() for unit in self._layer]
        ):
            try:
                await t
            except asyncio.CancelledError:
                pass
            except Exception as e:
                self._task_manager.call_exception_handler({
                    "message": str(e),
                    'exception': e
                })
                break

        logger.debug("{} is done with processing".format(self._name))

    async def _shut_channels(self):
        for fut in asyncio.as_completed([
            c.shutdown(True)
            for c in self._hidden_connections + self._connections
        ]):
            await fut


class ParallelLayer(Layer):
    def add_unit(
        self, unit, additional_inputs=(), additional_outputs=(), bcast=False
    ):
        super().add_unit(
            unit,
            (self.input,) + additional_inputs,
            (self.output,) + additional_outputs,
            bcast
        )


class SequenceLayer(Layer):
    def initialize(self, exception_handler=None, depth=0, **kwargs):
        self._layer[-1].connect_output(self.output)
        super().initialize(exception_handler, depth)

    def add_unit(
        self, unit, additional_inputs=(), additional_outputs=(), bcast=False
    ):
        if len(self._layer) == 0:
            input = (self.input,)
        else:
            if len(self._hidden_connections) % 2 == 0:
                input = (self._hidden_connections[-2],)
            else:
                input = (self._hidden_connections[-1],)

        super().add_unit(
            unit, additional_inputs + input, additional_outputs, bcast
        )


logger = logging.getLogger(Layer.__name__)
