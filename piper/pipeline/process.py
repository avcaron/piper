from abc import ABCMeta, abstractmethod
from os.path import join

from piper.drivers.shell import launch_shell_process
from piper.graph.serializable import Serializable
from piper.utils.config import piper_config


class Process(Serializable, metaclass=ABCMeta):
    def __init__(self, name, output_path, prefix, input_keys=(), optional_keys=()):
        super().__init__()
        self._name = name
        self._output_package = {
            "path": output_path,
            "prefix": prefix
        }
        self._input_keys = input_keys
        self._opt_keys = optional_keys
        self._input = None
        self._metadata = None
        self._n_cores = 1

    @property
    def serialize(self):
        return {**super().serialize, **{
            'name': self.name,
            'required_inputs': self._input_keys,
            'optional_inputs': self._opt_keys,
            'n_cores': self._n_cores,
            'process_executor': str(self._launch_process)
        }}

    @property
    def launcher(self):
        return self._launch_process

    @property
    def primary_input_key(self):
        return self.input_keys[0]

    @property
    def input_keys(self):
        return self._input_keys

    @property
    @abstractmethod
    def required_output_keys(self):
        pass

    @property
    def name(self):
        return self._name

    @property
    def prefix(self):
        return self._output_package["prefix"]

    @property
    def path_prefix(self):
        return self._output_package["path"]

    @property
    def output_prefix(self):
        return join(
            self._output_package["path"],
            self._output_package["prefix"]
        )

    @property
    def n_cores(self):
        return self._n_cores

    @n_cores.setter
    def n_cores(self, n_cores):
        self._n_cores = n_cores

    @property
    def metadata(self):
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        self._metadata = metadata

    def set_inputs(self, package):
        pkg = self._fill_optional(package)
        self._input = [pkg[key] for key in self._input_keys]

    def execute(self, *args, **kwargs):
        result = self._launch_process(self._execute, *args, **kwargs)
        if result and isinstance(result, dict):
            self._output_package.update(result)

    def get_outputs(self):
        return self._output_package

    def override_process_launcher(self, launcher):
        self._launch_process = launcher
        return self

    @abstractmethod
    def _execute(self, *args, **kwargs):
        pass

    @abstractmethod
    def _launch_process(self, runnable, *args, **kwargs):
        pass

    def _fill_optional(self, package):
        return {**{opt: None for opt in self._opt_keys}, **package}


class PythonProcess(Process, metaclass=ABCMeta):
    def _launch_process(self, py_method, *args, **kwargs):
        return py_method(*args, **kwargs)


class ShellProcess(Process, metaclass=ABCMeta):
    def _launch_process(self, command, log_file, *args, **kwargs):
        log_conf = piper_config.generate_shell_log_config()
        return launch_shell_process(
            piper_config.format_shell_executable(command(
                *args, **kwargs
            )),
            log_file, **log_conf
        )
