from abc import ABC, abstractmethod, ABCMeta
from uuid import uuid4


class Serializable(ABC, metaclass=ABCMeta):
    def __init__(self):
        self._uuid = uuid4()

    @property
    @abstractmethod
    def serialize(self):
        return {
            'uuid': self._uuid,
            'type': type(self)
        }
