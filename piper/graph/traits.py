from enum import Enum

import piper.graph.models as models
from piper import pipeline, comm


class ModelTraits(Enum):
    CHANNEL = (models.Channel, comm.Channel)
    SUBSCRIBER = (models.Subscriber, comm.subscriber)
    PROCESS = (models.Process, pipeline.Process)
    UNIT = (models.Unit, pipeline.Unit)
    LAYER = (models.Layer, pipeline.Layer)
    PIPELINE = (models.Pipeline, pipeline.Pipeline)


def get_all_parents(obj_klass):
    parents = obj_klass.__bases__
    if len(parents) > 0 and parents[0] is not object:
        return parents + tuple(
            pp for p in parents for pp in get_all_parents(p)
        )

    return tuple()


def get_associated_model(obj_klass):
    related_traits = list(filter(
        lambda trait: trait.value[1] in get_all_parents(obj_klass),
        list(ModelTraits)
    ))

    p_len = len(related_traits) + 1
    while len(related_traits) > 1 and not len(related_traits) == p_len:
        p_len = len(related_traits)

        related_traits = list(filter(
            lambda trait: not any([
                t.value[1] in trait.__bases__
                for t in filter(lambda oth: oth is not trait, related_traits)
            ]
            ), related_traits)
        )

    return related_traits[0].value[0]
