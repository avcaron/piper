import asyncio
from multiprocessing import cpu_count

from psutil import virtual_memory


class ResourcesRequest:
    cpu = 1
    mem = 0

    def __iter__(self):
        return iter([self.cpu, self.mem])


class GlobalAllocation:
    __keys__ = ["cpu", "mem"]
    cpu = cpu_count()
    mem = virtual_memory()
    usage = {
        "cpu": 0,
        "mem": 0
    }

    def __iter__(self):
        return iter([self.cpu, self.mem.total])

    def satisfies(self, request):
        cond = all(
            max - self.usage[k] - req >= 0
            for k, req, max in zip(self.__keys__, iter(request), iter(self))
        )

        if cond:
            for k, req in zip(self.__keys__, iter(request)):
                self.usage[k] += req

        return cond

    def deallocate(self, request):
        for k, req in zip(self.__keys__, iter(request)):
            self.usage[k] -= req


class ResourcesAllocator:
    class _Allocator:
        def __init__(self, cond_fn, request, ref_alloc):
            self._cond = cond_fn
            self._request = request
            self._ref_alloc = ref_alloc

        async def __aenter__(self):
            await asyncio.Condition().wait_for(self._cond)

        async def __aexit__(self, exc_type, exc_val, exc_tb):
            self._ref_alloc.deallocate(self._request)

    def __init__(self, allocation=GlobalAllocation()):
        self._allocation = allocation
        self._waiting_exes = []

    def allocate(self, request):
        return self._Allocator(
            lambda: self._allocation.satisfies(request),
            request, self._allocation
        )
