import importlib_metadata as metadata

from .resources.resources_allocator import ResourcesAllocator
from .utils.metadata import MetadataManager

try:
    __version__ = metadata.version(__name__)
except metadata.PackageNotFoundError:
    print("Package {} not found".format(__name__))
    pass


allocator = ResourcesAllocator()
metadata_manager = MetadataManager()
