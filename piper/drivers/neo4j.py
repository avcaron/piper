from neomodel import config, db, install_all_labels

from piper.utils.config import piper_config


def db_config():
    return {
        'url': str(piper_config.neo4j_db_connection)
    }


def transaction_manager():
    return db.transaction


def config_db():
    config.DATABASE_URL = str(piper_config.neo4j_db_connection)
    config.ENCRYPTED_CONNECTION = False


def init_db():
    install_all_labels()

