import asyncio
import logging

from abc import abstractmethod
from functools import partial
from threading import Event

from .thread import ThreadManager
from piper.utils.config import piper_config


async def _async_wrapper(func, *args, **kwargs):
    return func(*args, **kwargs)


def _queue_task_from_other_thread(coro, loop):
    return asyncio.wrap_future(asyncio.run_coroutine_threadsafe(coro, loop))


async def _cancel_in_loop(future):
    return await _queue_task_from_other_thread(
        _async_wrapper(future.cancel), future.get_loop()
    )


class _TaskManager:
    def __init__(self, loop):
        self._loop = loop
        self._protected = []

    @property
    def loop(self):
        return self._loop

    def is_running(self):
        return self.loop and self.loop.is_running()

    def is_closed(self):
        return not self.is_running() and (self.loop and self.loop.is_closed())

    def is_current_loop(self):
        try:
            return self.loop is asyncio.get_event_loop()
        except RuntimeError:
            return True

    def create_task(self, coro, protected=False, done_cbk=None):
        if self.is_current_loop():
            awaitable = self.loop.create_task(coro)
            if done_cbk:
                awaitable.add_done_callback(done_cbk)
        else:
            awaitable = _queue_task_from_other_thread(
                self._create_task_for_coro(coro, protected, done_cbk),
                self.loop
            )

        if protected:
            self._protected.append(awaitable)

        return awaitable

    def protect_task(self, task):
        self._protected.append(task)
        return task

    def create_protected_task(self, coro):
        return self.protect_task(self.create_task(coro, True))

    def protect_current_task(self):
        return self.protect_task(asyncio.current_task(self.loop))

    def wrap_sync_func_in_task(self, func, *args, protected=False, **kwargs):
        return self.create_task(
            _async_wrapper(func, *args, **kwargs), protected
        )

    def run_loop(self, until_task=None):
        if until_task:
            self.loop.run_until_complete(until_task)
        else:
            self.loop.run_forever()

    def run_loop_until_empty(self):
        if self.loop:
            assert not self.is_running(), \
                "In order to clear the loop, it must be stopped first"

            self.run_loop(self.create_protected_task(self._clear()))

    def close(self):
        if self.loop.is_running():
            assert len(asyncio.all_tasks(self.loop)) == 0, \
                "Loop must be cleared before closing"

            self.loop.stop()

        self.loop.close()

    def call_exception_handler(self, context):
        self.loop.call_exception_handler(context)

    async def cancel_running_tasks(self, excluding=()):
        assert isinstance(excluding, tuple)
        self.protect_current_task()
        excluding += tuple(self._protected)

        running_tasks = list(filter(
            lambda task: not task.done() and task not in excluding,
            asyncio.all_tasks(self.loop)
        ))

        if len(running_tasks) > 0:
            return await self.cancel_tasks(running_tasks)

        return True

    async def cancel_tasks(self, tasks):
        real_tasks = list(filter(lambda t: isinstance(t, asyncio.Task), tasks))
        all_cancelled = all([task.cancel() for task in real_tasks])

        futures = filter(lambda t: not isinstance(t, asyncio.Task), tasks)
        fut_cancelled = all([await _cancel_in_loop(f) for f in futures])

        if len(real_tasks) > 0:
            await asyncio.wait(real_tasks)

        return all_cancelled and fut_cancelled

    async def _clear(self):
        curr_task = self.protect_current_task()

        while True:
            tasks = list(filter(
                lambda t: t is not curr_task,
                asyncio.all_tasks(self.loop)
            ))

            if len(tasks) == 0:
                break

            await asyncio.wait(tasks, loop=self.loop)

        # self._protected.clear()

    async def _create_task_for_coro(
        self, coro, protected=False, done_cbk=None
    ):
        task = self.loop.create_task(coro)
        if done_cbk:
            task.add_done_callback(done_cbk)

        if protected:
            self.protect_task(task)
        try:
            return await task
        except asyncio.CancelledError as e:
            task.cancel()
            raise e


class AsyncLoopManager(ThreadManager):
    class _DoneEvent:
        ...

    def __init__(self, name="AsyncLoopManager"):
        super().__init__(name)
        self._done = AsyncLoopManager._DoneEvent()
        self._ready = Event()
        self._closing = False
        self._task_manager = None
        self._base_exception_handler = None

        self._add_callback_categories(['ready', 'start', 'done', 'close'])

    @property
    def serialize(self):
        return {**super().serialize, **{
            'started': self.is_running(),
            'ended': self._done.done()
        }}

    def is_running(self):
        return self._task_manager and self._task_manager.is_running()

    def is_done(self):
        return self._done.done()

    def is_closed(self):
        return self._task_manager.is_closed()

    def add_started_callback(self, fn):
        self._callbacks['start'].append(fn)

    def add_ready_callback(self, fn):
        self._callbacks['ready'].append(fn)

    def add_done_callback(self, fn):
        self._callbacks['done'].append(fn)

    def add_close_callback(self, fn):
        self._callbacks['close'].append(fn)

    def start(
        self, *args, daemon=True, exception_handler=None, **kwargs
    ):
        self._base_exception_handler = exception_handler

        super().start(daemon=daemon)
        return self._ready

    def launch_async_executable(self, *args, **kwargs):
        self._task_manager.create_protected_task(
            self._execute_async(*args, **kwargs)
        )

    def close_loop(self):
        if not self._done.done():
            self._trigger_callbacks('done')
            self._task_manager.create_task(self._done.set(), True)

    def _execute_threaded(self):
        super()._execute_threaded()

        logger.debug("{} starting async loop".format(self._name))
        loop = asyncio.new_event_loop()
        loop.set_exception_handler(partial(
            self._async_exception_handler,
            basic_handler=self._base_exception_handler
        ))
        loop.set_debug(piper_config.debug)
        self._task_manager = _TaskManager(loop)

        logger.debug("{} async loop is running".format(self._name))

        if len(self._callbacks['start']) > 0:
            self._task_manager.wrap_sync_func_in_task(
                self._trigger_callbacks, 'start'
            )

        self._done.initialize(self._task_manager)
        self._ready.set()
        self._trigger_callbacks('ready')

        closing_task = self._task_manager.create_protected_task(
            self._done.wait_on_future()
        )

        self._task_manager.run_loop(closing_task)
        logger.debug(
            "{} loop has received a shutdown call".format(self.name)
        )
        self._task_manager.run_loop_until_empty()
        self._task_manager.close()
        self._trigger_callbacks('close')
        logger.debug("{} async loop closed".format(self.name))

        if self._thread.daemon:
            self.stop(False)

    def _async_exception_handler(self, loop, context, base_handler=None):
        exception = context["exception"] if "exception" in context else None
        logger.warning("{} loop received an exception".format(self.name))
        if exception:
            logger.exception(str(exception))
        if base_handler:
            base_handler(loop, context)

    @abstractmethod
    async def _execute_async(self, *args, **kwargs):
        pass

    async def _cancel_running_tasks(self, excluding=()):
        self._task_manager.protect_current_task()
        return await self._task_manager.cancel_running_tasks(excluding)

    class _DoneEvent:
        def __init__(self):
            self._event = None
            self._t_man = None

        async def wait_on_future(self):
            self._t_man.protect_current_task()
            await self._t_man.create_protected_task(self._protected_wait())

        def initialize(self, task_manager):
            self._t_man = task_manager
            self._event = asyncio.Event(loop=task_manager.loop)

        def get_loop(self):
            return self._t_man.loop

        async def set(self):
            if not (self.done() or self.get_loop().is_closed()):
                await self._t_man.protect_task(
                    self._t_man.wrap_sync_func_in_task(self._event.set)
                )

        def done(self):
            return self._event is not None and self._event.is_set()

        async def _protected_wait(self):
            await self._t_man.create_protected_task(self._event.wait())


logger = logging.getLogger(AsyncLoopManager.__name__)
