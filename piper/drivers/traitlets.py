from abc import abstractmethod, ABCMeta
from os.path import exists, isfile, isdir

from traitlets import Unicode, TCPAddress, TraitType, Instance, Bool
from traitlets.config import Configurable


class SelfInstantiatingInstance(Instance):
    def validate(self, obj, value):
        if isinstance(value, dict):
            return self.klass(**value)
        elif value is None:
            try:
                return self.klass()
            except BaseException:
                self.error(obj, value)
        return super().validate(obj, value)


class ExistingFile(TraitType):
    def _validate(self, obj, value):
        try:
            if isfile(value) and exists(value):
                return value
            self.error(obj, value)
        except BaseException:
            if value is None:
                return value
            self.error(obj, value)


class ExistingDirectory(TraitType):
    def _validate(self, obj, value):
        try:
            if isdir(value) and exists(value):
                return value
            self.error(obj, value)
        except BaseException:
            if value is None:
                return value
            self.error(obj, value)


class PiperConfigurable(Configurable):
    @abstractmethod
    def to_json_configuration(self):
        pass


class Connection(PiperConfigurable):
    username = Unicode().tag(config=True)
    password = Unicode().tag(config=True)
    address = TCPAddress().tag(config=True)
    encrypted = Bool(default_value=True).tag(config=True)
    protocol = Unicode(read_only=True)

    def __init__(self, **kwargs):
        if 'address' in kwargs and not isinstance('address', tuple):
            kwargs['address'] = tuple(kwargs['address'])
        super().__init__(**kwargs)

    def to_json_configuration(self):
        return {
            'username': self.username,
            'password': self.password,
            'address': self.address,
            'encrypted': self.encrypted
        }

    def __str__(self):
        return "{}://{}:{}@{}:{} -- encrypted : {}".format(
            self.protocol, self.username, self.password,
            *self.address, self.encrypted
        )


class BoltConnection(Connection):
    protocol = Unicode(default_value="bolt", read_only=True)


class HttpConnection(Connection):
    protocol = Unicode(default_value="http", read_only=True)


class SSHConnection(Connection):
    protocol = Unicode(default_value="ssh", read_only=True)
    password = None
    encrypted = None

    def __str__(self):
        return "{} {}@{}:{}".format(
            self.protocol, self.username, *self.address
        )

    def to_json_configuration(self):
        return {
            'username': self.username,
            'address': str(self.address)
        }
